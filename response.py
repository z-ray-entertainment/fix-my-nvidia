import datetime
from enum import Enum

from utils.config import config
from utils.i18n import i18n_dictionary


class LogType(Enum):
    INFO = "INFO"
    ERROR = "ERROR"
    FATAL = "FATAL ERROR"
    DEBUG = "DEBUG"
    WARN = "WARNING"


def log(log_type: LogType, messages):
    now = datetime.datetime.now()
    if isinstance(messages, list):
        for s in range(0, len(messages)):
            msg = str(now) + " [" + log_type.value + "]: " + s
            print(msg)
            if config.get_property("write_log"):
                _append_to_log(msg)
    else:
        msg = str(now) + " [" + log_type.value + "]: " + messages
        print(msg)
        if config.get_property("write_log"):
            _append_to_log(msg)


def tell(message_key):
    if isinstance(message_key, list):
        for s in range(0, len(message_key)):
            print(i18n_dictionary.get_text(s))
    else:
        print(i18n_dictionary.get_text(message_key))


def _append_to_log(text: str):
    pass
