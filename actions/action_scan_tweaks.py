import response
from detector.nvidia import has_nv_gpu


def scan_tweaks():
    if has_nv_gpu():
        response.tell("response_no_tweaks_found")
    else:
        response.tell("response_no_nv_gpu")
        response.tell("response_no_tweaks_found")

