import gi

import const

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MainFrame(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title=const.TITLE + " " + const.VERSION)

        self.grid = Gtk.Grid()
        self.add(self.grid)

        self.label = Gtk.Label(label=const.TITLE)
        self.button = Gtk.Button(label="Click Here")
        self.button.connect("clicked", self.on_button_clicked)
        self.grid.add(self.label)
        self.grid.add(self.button)

    def on_button_clicked(self, widget):
        print("Hello World")
