import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from ui.main_frame import MainFrame

def run_ui():
    main_frame = MainFrame()
    main_frame.connect("destroy", Gtk.main_quit)
    main_frame.show_all()
    Gtk.main()