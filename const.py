import locale
import os

CONST = {}

CONST["APP_TITLE"] = "Fix my NVidia"
CONST["APP_VERSION"] = "0.1"
CONST["GTK_INTERFACE_SUPPORTED"] = False
CONST["ASSISTANT_NAME"] = "Hurby"
CONST["OS_LANG"] = locale.getdefaultlocale()[0]
CONST["OS_ENCODING"] = locale.getdefaultlocale()[1]

CONST["DIR_HOME_ABSOLUTE"] = os.getenv("HOME")
CONST["DIR_APP_DATA_ABSOLUTE"] = CONST["DIR_HOME_ABSOLUTE"] + "/.local/share/fix-my-nvidia/"
CONST["DIR_APP_PATH_ABSOLUTE"] = os.getcwd()

CONST["FILE_CONFIG_ABSOLUTE"] = CONST["DIR_APP_DATA_ABSOLUTE"] + "config.json"
CONST["FILE_LOGFILE_ABSOLUTE"] = CONST["DIR_APP_DATA_ABSOLUTE"] + "log.log"
CONST["SUDO"] = "sudo"
