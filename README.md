## Fix my Nvidia

### About
This is a collection of shell and python 3 scripts which do support a UI as well  
which aim to fix various known issues with the binary Nvidia Linux driver.

### What will it support?

#### Wayland
This script will automatically update various system configurations to enable  
NVidia useres to use the new Wayland display protocoll.  
Pleas keep in mind that wayland is not fully supported by nvidia themself and  
therefore we can not fix everything.  
This said wayland do work on Linux using Gnome 3 and Plasma 5 but with out XWayland  
hardware acceleration.  
Even if you can afterwards use Wayland be prepared for very poor performance  

#### Tearing fix
THis project will allow you to one click fix NVidia teraing issues for your   
currently used dekstop environment.  
Initially supported DEs will be KDE and Gnome.  

#### Hibernate
On some configurations hibernate on NVidia GPUs causes unwanted dekstop behaviour  
after resuming the session. This script will fix this as well.  

#### Many more
This will be of course updates the mment we know more issues which can be fixed  
and as soon as we know how to do so.  