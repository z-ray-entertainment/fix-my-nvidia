import os


def has_binary(binary_name) -> dict:
    result = {
        "exists": True,
        "root_access": False
    }
    if os.path.isfile("/bin/" + binary_name):
        return result
    elif os.path.isfile("/sbin/" + binary_name):
        result["root_access"] = True
        return result
    else:
        result = {
            "exists": False,
            "root_access": False
        }
