import subprocess

from const import CONST
from detector.binary import has_binary
from utils import run_shell_cmd

_lspci_root_access = False
_grep_nv_gpus_cmd = "lspci -vnn"


def has_nv_gpu():
    _check_lspci()
    if _lspci_root_access:
        result = run_shell_cmd.run_shell_cmd(CONST["SUDO"] + " " + _grep_nv_gpus_cmd)
    else:
        result = run_shell_cmd.run_shell_cmd(_grep_nv_gpus_cmd)
    print(result)


def _check_lspci():
    result = has_binary("lspci")
    global _lspci_root_access
    _lspci_root_access = result["root_access"]
