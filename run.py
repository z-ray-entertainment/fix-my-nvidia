from cli import run_cli
from const import CONST
from detector import session
from ui import run_ui

if not CONST["GTK_INTERFACE_SUPPORTED"] or session.is_headless_session():
    run_cli.run_cli()
else:
    run_ui.run_ui()
