import os

import response
from actions import action_quit, action_scan_tweaks
from const import CONST
from utils.i18n import i18n_dictionary
from utils.json import json_hanlder

_action_list = {}
_cur_item_num = -1
_cta_text = ""
_current_menu = ""


def call_menu(menu_name: str):
    menu_json = json_hanlder.load_json(CONST["DIR_APP_PATH_ABSOLUTE"] + "/json/menu/cli/" + menu_name + ".json")
    global _current_menu
    _current_menu = menu_name
    _build_title(menu_json["title"])
    _build_items(menu_json["items"])
    print("")
    global _cta_text, _cur_item_num
    _cur_item_num = -1
    _cta_text = _replace_all(menu_json["cta"])


def mail_loop():
    while True:
        user_input = input(_cta_text)
        func = _action_list[user_input]
        os.system('clear')
        call_menu(_current_menu)
        _call_action(func)


def _build_title(title: str):
    print(_replace_all(title))


def _build_items(items: dict):
    global _cur_item_num
    for i in items:
        _cur_item_num += 1
        if isinstance(items[i]["visible"], bool):
            if items[i]["visible"]:
                if items[i]["type"] == "label":
                    _build_label(items[i])
                elif items[i]["type"] == "action":
                    _build_action(items[i], i)
        else:
            pass


def _build_label(label):
    print(_replace_all(label["text"]))


def _build_action(action, action_name):
    if action["action"].startswith("?") and action["action"].endswith("?"):
        action_event = action["action"][1:len(action["action"]) - 1]
        action_key_text = _replace_ui(action["action_key"])
        action_text = _replace_all(action["text"])
        _action_list[str(action_key_text)] = action_event

        print(str(action_key_text) + "): " + action_text)
    else:
        response.log(response.LogType.WARN, "Action item does not have an action: " + action_name)


def _replace_all(text):
    text = _replace_i18n(text)
    text = _replace_constants(text)
    return text


def _replace_i18n(text: str):
    keys = text.split(" ")
    replaced_text = ""
    for k in keys:
        if k.startswith("$") and k.endswith("$"):
            i18n_tag = k[1:len(k) - 1]
            i18n_text = i18n_dictionary.get_text(i18n_tag)
            replaced_text += i18n_text + " "
        else:
            replaced_text += k + " "
    return replaced_text


def _replace_constants(text: str):
    words = text.split(" ")
    replaced_text = ""
    for w in words:
        if w.startswith("$") and not w.endswith("$"):
            key = w[1:len(w)]
            replaced_text += CONST[key.upper()] + " "
        else:
            replaced_text += w + " "
    return replaced_text


def _replace_ui(text: str):
    words = text.split(" ")
    replaced_text = ""
    for w in words:
        if w.startswith("#") and w.endswith("#"):
            key = w[1:len(w) - 1]
            if key == "item_num":
                replaced_text = _cur_item_num
        else:
            replaced_text += w + " "
    return replaced_text


def _call_action(key):
    if key == "action_quit.exit_application":
        action_quit.exit_application()
    elif key == "action_scan_tweaks.scan_tweaks":
        action_scan_tweaks.scan_tweaks()
