import response
from cli.menus import cli_handler


def run_cli():
    response.tell("found_headless")
    # response.log(response.LogType.INFO, "No display found running cli")
    cli_handler.call_menu("main")
    cli_handler.mail_loop()
