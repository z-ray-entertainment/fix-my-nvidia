import shlex
import subprocess


def run_shell_cmd(cmd):
    cmds = shlex.split(cmd)
    r = subprocess.run(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result = {
        "out": r.stdout.decode("utf-8"),
        "err": r.stderr.decode("utf-8")
    }
    return result
