import const

_replacements = {
    "$app_version": const.VERSION,
    "$app_title": const.TITLE,
    "$assistant_name": const.ASSISTANT_NAME
}


def replace_keys(text_in: str):
    text_out = text_in
    for s in _replacements:
        text_out = text_in.replace(s, _replacements[s])
    return text_out
